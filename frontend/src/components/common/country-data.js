const options = [
    {
      value: '北京市',
      label: '北京市',
      children: [
        {
          value: '朝阳区',
          label: '朝阳区',
          children: [
            { value: '北华街', label: '北华街' },
            { value: '北三环南桥', label: '北三环南桥' },
            { value: '北三环北桥', label: '北三环北桥' },
            { value: '北三环西桥',label: '北三环西桥' }
          ]
        }
      ]
    },
    {
        value: '天津市',
        label: '天津市',
        children: [
          {
            value: '和平区',
            label: '和平区',
            children: [
              { value: '北辰街', label: '北辰街' },
              { value: '北辰西街', label: '北辰西街' },
              { value: '北辰东街', label: '北辰东街' },
              { value: '北辰南街', label: '北辰南街' }
            ]
          }
        ]
    },
    {
        value: '河北省',
        label: '河北省',
        children: [
          {
            value: '石家庄市',
            label: '石家庄市',
            children: [
              { value: '长安街', label: '长安街' },
              { value: '裕华街', label: '裕华街' },
              { value: '井陉南街', label: '井陉南街' },
              { value: '井陉北街', label: '井陉北街' }
            ]
         },
        ]
    },
  {
    value: '广东省',
    label: '广东省',
    children: [
      {
        value: '广州市',
        label: '广州市',
        children: [
          {
            value: '天河区',
            label: '天河区',
            children: [
              { value: '天河南街', label: '天河南街' },
              { value: '石牌街', label: '石牌街' },
              { value: '兴华街', label: '兴华街' },
              { value: '元岗街', label: '元岗街' }
            ]
          },
          {
            value: '越秀区',
            label: '越秀区',
            children: [
              { value: '北京街', label: '北京街' },
              { value: '东山街', label: '东山街' },
              { value: '梅花村街', label: '梅花村街' },
              { value: '珠光街', label: '珠光街' }
            ]
          },
          {
            value: '海珠区',
            label: '海珠区',
            children: [
              { value: '昌岗街', label: '昌岗街' },
              { value: '江南中街', label: '江南中街' },
              { value: '凤阳街', label: '凤阳街' },
              { value: '赤岗街', label: '赤岗街' }
            ]
          }
        ]
      },
      {
        value: '深圳市',
        label: '深圳市',
        children: [
          {
            value: '福田区',
            label: '福田区',
            children: [
              { value: '福保街道', label: '福保街道' },
              { value: '莲花街道', label: '莲花街道' },
              { value: '香蜜湖街道', label: '香蜜湖街道' },
              { value: '梅林街道', label: '梅林街道' }
            ]
          },
          {
            value: '南山区',
            label: '南山区',
            children: [
              { value: '粤海街道', label: '粤海街道' },
              { value: '蛇口街道', label: '蛇口街道' },
              { value: '南山街道', label: '南山街道' },
              { value: '西丽街道', label: '西丽街道' }
            ]
          },
          {
            value: '宝安区',
            label: '宝安区',
            children: [
              { value: '新安街道', label: '新安街道' },
              { value: '西乡街道', label: '西乡街道' },
              { value: '福永街道', label: '福永街道' },
              { value: '沙井街道', label: '沙井街道' }
            ]
          }
        ]
      },
      {
        value: '佛山市',
        label: '佛山市',
        children: [
          {
            value: '禅城区',
            label: '禅城区',
            children: [
              { value: '祖庙街道', label: '祖庙街道' },
              { value: '石湾镇街道', label: '石湾镇街道' },
              { value: '张槎街道', label: '张槎街道' },
              { value: '南庄镇', label: '南庄镇' }
            ]
          },
          {
            value: '南海区',
            label: '南海区',
            children: [
              { value: '桂城街道', label: '桂城街道' },
              { value: '狮山镇', label: '狮山镇' },
              { value: '大沥镇', label: '大沥镇' },
              { value: '丹灶镇', label: '丹灶镇' }
            ]
          }
        ]
      }
    ]
  },
  {
    value: '江苏省',
    label: '江苏省',
    children: [
      {
        value: '南京市',
        label: '南京市',
        children: [
          {
            value: '玄武区',
            label: '玄武区',
            children: [
              { value: '梅园新村街道', label: '梅园新村街道' },
              { value: '玄武湖街道', label: '玄武湖街道' },
              { value: '锁金村街道', label: '锁金村街道' },
              { value: '孝陵卫街道', label: '孝陵卫街道' }
            ]
          },
          {
            value: '秦淮区',
            label: '秦淮区',
            children: [
              { value: '夫子庙街道', label: '夫子庙街道' },
              { value: '双塘街道', label: '双塘街道' },
              { value: '瑞金路街道', label: '瑞金路街道' },
              { value: '红花街道', label: '红花街道' }
            ]
          },
          {
            value: '鼓楼区',
            label: '鼓楼区',
            children: [
              { value: '湖南路街道', label: '湖南路街道' },
              { value: '宁海路街道', label: '宁海路街道' },
              { value: '中央门街道', label: '中央门街道' },
              { value: '江东街道', label: '江东街道' }
            ]
          }
        ]
      },
      {
        value: '苏州市',
        label: '苏州市',
        children: [
          {
            value: '姑苏区',
            label: '姑苏区',
            children: [
              { value: '平江街道', label: '平江街道' },
              { value: '沧浪街道', label: '沧浪街道' },
              { value: '双塔街道', label: '双塔街道' },
              { value: '金阊街道', label: '金阊街道' }
            ]
          },
          {
            value: '吴中区',
            label: '吴中区',
            children: [
              { value: '木渎镇', label: '木渎镇' },
              { value: '胥口镇', label: '胥口镇' },
              { value: '长桥街道', label: '长桥街道' },
              { value: '郭巷街道', label: '郭巷街道' }
            ]
          },
          {
            value: '相城区',
            label: '相城区',
            children: [
              { value: '元和街道', label: '元和街道' },
              { value: '黄埭镇', label: '黄埭镇' },
              { value: '渭塘镇', label: '渭塘镇' },
              { value: '望亭镇', label: '望亭镇' }
            ]
          }
        ]
      },
      {
        value: '无锡市',
        label: '无锡市',
        children: [
          {
            value: '梁溪区',
            label: '梁溪区',
            children: [
              { value: '崇安寺街道', label: '崇安寺街道' },
              { value: '清名桥街道', label: '清名桥街道' },
              { value: '广益街道', label: '广益街道' },
              { value: '黄巷街道', label: '黄巷街道' }
            ]
          },
          {
            value: '锡山区',
            label: '锡山区',
            children: [
              { value: '东亭街道', label: '东亭街道' },
              { value: '安镇街道', label: '安镇街道' },
              { value: '鹅湖镇', label: '鹅湖镇' },
              { value: '羊尖镇', label: '羊尖镇' }
            ]
          }
        ]
      }
    ]
  }
];

export default options
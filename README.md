# EcoNest

**<font color=red>注意</font>：此项目正在开发中，暂不支持本地或远程调试运行！**

#### 一、项目介绍

EcoNest 是一个基于 Spring Boot 和 Vue 的前后端分离二手交易平台，象征着用 “Nest” 比喻温馨的环境，体现共同守护地球的家园。

#### 二、项目目标

“未来创想，网启新篇”。

#### 三、项目主题

**持续生活**：倡导绿色生活方式，设计平台推广环保理念，功能包括二手物品交换、环保产品购买及碳足迹计算等。鼓励用户积极采取环保措施，共同守护地球家园，实现可持续发展。

#### 四、所用技术

**后端技术栈**：

- **Spring Boot**：基于 Spring 框架的快速开发框架，简化 Java 应用程序的配置与开发，提供开箱即用的功能。
- **MySQL**：流行的开源关系数据库管理系统，具备高性能、可靠性和可扩展性，适合存储和管理数据。
- **MyBatis**：持久层框架，支持自定义 SQL 、存储过程及高级映射，简化数据库操作。
- **MyBatis-Plus**：MyBatis 的增强工具，提供便捷方法简化 CRUD 操作，提高开发效率。
- **Hutool**：Java 工具类库，提供常用工具，简化开发中的常见任务，如文件操作和 HTTP 请求。
- **Lombok**：Java 库，通过注解减少样板代码，简化类的定义，特别是 Getter/Setter 和构造函数。

**前端技术栈**：

- **HTML、CSS、JavaScript**：构建网页的基础技术，HTML 负责结构，CSS 负责样式，JavaScript 提供动态功能。
- **Vue**：渐进式 JavaScript 框架，用于构建用户界面，支持组件化开发，易于学习和灵活集成。
- **vue-router**：Vue 的官方路由管理器，支持单页面应用中的多视图导航，管理页面状态和历史记录。
- **vuex**：Vue 的状态管理库，集中管理应用状态，确保组件间的数据共享和一致性。
- **Vite**：快速的前端构建工具，利用原生ES模块，提供超快的开发体验和构建性能，支持热模块替换。
- **Axios**：基于 Promise 的 HTTP 客户端，简化与后端 API 的交互，支持发送异步请求。
- **Apache ECharts**：开源图表库，提供丰富的图表类型和交互效果，适合数据可视化。
- **Element Plus**：基于 Vue 3 的桌面端组件库，提供丰富的 UI 组件，帮助快速构建美观的用户界面。

#### 五、环境介绍

**编译环境**：

- 后端：[IDEA](https://www.jetbrains.com/zh-cn/idea/)
- 前端：[WebStorm](https://www.jetbrains.com/zh-cn/webstorm/)

**基础环境**：

- [JDK 17](https://www.oracle.com/hk/java/technologies/downloads/#java17) 及以上
- [MySQL 8.0.39](https://dev.mysql.com/downloads/mysql/) 及以上
- [Maven 3.9.9](https://maven.apache.org/download.cgi)
- [Node.js 20](https://nodejs.org/zh-cn/download/package-manager)

#### 六、部署教程

1. 在选定文件夹下打开命令行窗口，执行 `git clone git@gitee.com:hongmengchukai/eco-nest.git` 将 EcoNest 克隆到本地。
2. 使用 [DataGrip](https://www.jetbrains.com/zh-cn/datagrip/) 或其他工具，在 MySQL 中创建名为 eco_nest 的数据库，并执行 sql 文件夹下的 SQL 文件。
3. 使用 IDEA 打开 EcoNest 项目下的 backend（后端）文件夹，下载 Maven 依赖项，等待下载完成。
4. 进入 backend/src/main/resources/，将 application.properties.bak 文件重命名为 application.properties ，并修改数据库配置和图片路径为自己的命名与路径。
5. 启动 backend/src/main/java/cn/codetrio/backend/ 中的主类 BackendApplication.java。
6. 使用 WebStorm 打开 EcoNest 项目下的 frontend（前端）文件夹，执行 npm install，下载完成后执行相关命令。 

如有其他需要，请随时告诉我！
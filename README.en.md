# EcoNest

**<font color=red>Note</font>: This project is currently in development and does not support local or remote debugging!**

#### 1. Project Introduction

EcoNest is a front-end and back-end separated second-hand trading platform based on Spring Boot and Vue, symbolizing a warm environment with "Nest" and reflecting the shared responsibility of protecting our planet.

#### 2. Project Goals

"Future Innovations, New Chapters Online."

#### 3. Project Theme

**Sustainable Living**: Advocating for a green lifestyle, the platform is designed to promote environmental protection concepts, including functions for exchanging second-hand goods, purchasing eco-friendly products, and calculating carbon footprints. Users are encouraged to actively take environmental measures to jointly protect the Earth and achieve sustainable development.

#### 4. Technologies Used

**Back-end Tech Stack**:

- **Spring Boot**: A rapid development framework based on the Spring framework that simplifies the configuration and development of Java applications, providing out-of-the-box functionality.
- **MySQL**: A popular open-source relational database management system with high performance, reliability, and scalability, suitable for storing and managing data.
- **MyBatis**: A persistence layer framework that supports custom SQL, stored procedures, and advanced mapping, simplifying database operations.
- **MyBatis-Plus**: An enhancement tool for MyBatis that provides convenient methods to simplify CRUD operations and improve development efficiency.
- **Hutool**: A Java utility library that offers common tools to simplify common tasks during development, such as file operations and HTTP requests.
- **Lombok**: A Java library that reduces boilerplate code through annotations, simplifying class definitions, especially for getters/setters and constructors.

**Front-end Tech Stack**:

- **HTML, CSS, JavaScript**: The foundational technologies for building web pages, with HTML responsible for structure, CSS for styling, and JavaScript providing dynamic functionality.
- **Vue**: A progressive JavaScript framework used for building user interfaces, supporting component-based development, easy to learn, and flexible to integrate.
- **vue-router**: The official routing manager for Vue, supporting multi-view navigation in single-page applications and managing page state and history.
- **vuex**: A state management library for Vue that centralizes application state management, ensuring data sharing and consistency between components.
- **Vite**: A fast front-end build tool that leverages native ES modules, providing an ultra-fast development experience and build performance, supporting hot module replacement.
- **Axios**: A Promise-based HTTP client that simplifies interaction with back-end APIs and supports sending asynchronous requests.
- **Apache ECharts**: An open-source chart library that provides a variety of chart types and interactive effects, suitable for data visualization.
- **Element Plus**: A desktop component library based on Vue 3 that offers a rich set of UI components to help quickly build beautiful user interfaces.

#### 5. Environment Introduction

**Compilation Environment**:

- Back-end: [IDEA](https://www.jetbrains.com/zh-cn/idea/)
- Front-end: [WebStorm](https://www.jetbrains.com/zh-cn/webstorm/)

**Basic Environment**:

- [JDK 17](https://www.oracle.com/hk/java/technologies/downloads/#java17) and above
- [MySQL 8.0.39](https://dev.mysql.com/downloads/mysql/) and above
- [Maven 3.9.9](https://maven.apache.org/download.cgi)
- [Node.js 20](https://nodejs.org/zh-cn/download/package-manager)

#### 6. Deployment Tutorial

1. Open a command line window in the selected folder and execute `git clone git@gitee.com:hongmengchukai/eco-nest.git` to clone EcoNest locally.
2. Use [DataGrip](https://www.jetbrains.com/zh-cn/datagrip/) or other tools to create a database named `eco_nest` in MySQL and execute the SQL files in the sql folder.
3. Use IDEA to open the backend folder in the EcoNest project, download the Maven dependencies, and wait for the download to complete.
4. Go to `backend/src/main/resources/`, rename `application.properties.bak` to `application.properties`, and modify the database configuration and image paths to your own naming and paths.
5. Start the main class `BackendApplication.java` in `backend/src/main/java/cn/codetrio/backend/`.
6. Use WebStorm to open the frontend folder in the EcoNest project, execute `npm install`, and run the related commands after the download is complete.

If you need anything else, feel free to let me know!
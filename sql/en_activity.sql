-- 创建 en_activity 表
CREATE TABLE en_activity (
    -- 活动ID，主键，自增
    id INT PRIMARY KEY AUTO_INCREMENT,

    -- 活动标题，非空
    title VARCHAR(255) NOT NULL,

    -- 活动描述
    description TEXT,

    -- 活动碳值
    carbon_value DECIMAL(10, 2) NOT NULL DEFAULT 10,

    -- 活动开始日期
    start_date DATETIME NOT NULL,

    -- 活动结束日期
    end_date DATETIME NOT NULL,

    -- 活动地点
    location VARCHAR(255) NOT NULL
);

-- 插入6条数据
INSERT INTO en_activity (title, description, carbon_value, start_date, end_date, location) VALUES
('环保骑行活动', '组织一次环保骑行活动，倡导绿色出行', 15.50, '2023-10-01 08:00:00', '2023-10-01 12:00:00', '城市公园'),
('植树节活动', '参与植树节活动，为地球增添绿色', 20.00, '2023-11-15 09:00:00', '2023-11-15 17:00:00', '郊区林地'),
('垃圾分类宣传', '举办垃圾分类宣传活动，提高环保意识', 12.75, '2023-12-05 10:00:00', '2023-12-05 14:00:00', '社区广场'),
('海洋清洁行动', '组织海洋清洁行动，保护海洋环境', 18.25, '2024-01-20 07:30:00', '2024-01-20 16:00:00', '海滩'),
('节能减排讲座', '举办节能减排讲座，推广低碳生活', 10.00, '2024-02-10 14:00:00', '2024-02-10 16:00:00', '会议中心'),
('绿色建筑参观', '参观绿色建筑，学习环保建筑技术', 14.50, '2024-03-05 13:00:00', '2024-03-05 16:00:00', '绿色建筑园区');
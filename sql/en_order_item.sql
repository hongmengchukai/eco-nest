CREATE TABLE en_order_item (
    -- 订单商品ID，主键，自增
                               id INT AUTO_INCREMENT PRIMARY KEY,

    -- 订单ID，外键，引用 en_order 表的 id
                               order_id INT NOT NULL,

    -- 商品ID，外键，引用 en_product 表的 id
                               product_id INT NOT NULL,

    -- 商品数量，不为空
                               quantity INT NOT NULL,

    -- 商品单价，不为空
                               price DECIMAL(10, 2) NOT NULL,

    -- 每项订单的总金额 (数量 * 单价)，自动生成
                               total_price DECIMAL(10, 2) GENERATED ALWAYS AS (quantity * price) STORED,

    -- 外键约束
                               FOREIGN KEY (order_id) REFERENCES en_order(id) ON DELETE CASCADE,
                               FOREIGN KEY (product_id) REFERENCES en_product(id) ON DELETE CASCADE
);


INSERT INTO en_order_item (order_id, product_id, quantity, price)
VALUES
    (1, 1, 2, 199.99),
    (1, 2, 1, 59.99),
    (2, 3, 3, 29.99),
    (3, 4, 1, 39.99),
    (4, 5, 2, 79.99),
    (5, 6, 1, 299.99);


ALTER TABLE en_order_item ADD COLUMN created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP;
CREATE TABLE en_order (
    -- 订单ID，主键，自增
                          id INT AUTO_INCREMENT PRIMARY KEY,

    -- 用户账号，外键，引用 en_user 表的 account_number
                          user_account_number VARCHAR(255) NOT NULL,

    -- 订单编号，唯一，不为空
                          order_number VARCHAR(255) NOT NULL,

    -- 订单状态，默认为 'pending'
                          status ENUM('pending', 'completed', 'canceled') DEFAULT 'pending',

    -- 支付状态，默认为 '未支付'
                          payment_status ENUM('未支付', '已支付') DEFAULT '未支付',

    -- 支付时间
                          payment_time TIMESTAMP NULL,

    -- 支付方式，默认为 '支付宝'
                          payment_method ENUM('支付宝', '微信', '银行卡') DEFAULT '支付宝',

    -- 订单总价，不为空
                          total_price DECIMAL(10, 2) NOT NULL,

    -- 订单创建时间，自动设置为当前时间戳
                          created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,

    -- 外键约束
                          FOREIGN KEY (user_account_number) REFERENCES en_user(account_number) ON DELETE CASCADE
);

INSERT INTO en_order (user_account_number, order_number, status, payment_status, payment_method, total_price)
VALUES
    ('user001', 'ORD202401001', 'pending', '未支付', '支付宝', 150.00),
    ('user002', 'ORD202401002', 'pending', '未支付', '微信', 250.00),
    ('user003', 'ORD202401003', 'pending', '未支付', '银行卡', 250.00),
    ('user004', 'ORD202401004', 'completed', '已支付', '支付宝', 250.00),
    ('user005', 'ORD202401005', 'canceled', '未支付', '支付宝', 250.00);


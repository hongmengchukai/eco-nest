CREATE TABLE en_admin
(
    -- 主键，自增
    id INT AUTO_INCREMENT PRIMARY KEY,

    -- 管理员账号，不为空且唯一
    account_number varchar(255) NOT NULL UNIQUE,

    -- 管理员密码，不为空
    password varchar(255) NOT NULL,

    -- 管理员姓名（实名），不为空
    name varchar(255) NOT NULL,

    -- 创建时间，自动设置为当前时间戳
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO en_admin (account_number, password, name)
VALUES
('admin001', '123456', '陈亮'),
('admin002', '123456', '梁旭'),
('admin003', '123456', '李柯嘉');
CREATE TABLE en_carbon_footprint (
    -- 碳足迹ID，主键，自增
    id INT PRIMARY KEY AUTO_INCREMENT,

    -- 用户账号，外键，引用 en_users 表的 account_number，不为空
    user_id INT NOT NULL,

    -- 计算日期，不为空
    date_calculated DATETIME NOT NULL,

    -- 碳足迹值（单位：kg CO2e），不为空
    carbon_footprint DECIMAL(10, 2) NOT NULL,

    -- 购买的商品ID，外键，引用 en_products 表的 en_productID，不为空
    product_id INT NOT NULL,

    -- 参与活动ID，外键，引用 en_activity 表的 activity_ActivityID，不为空
    activity_id INT NOT NULL,

    FOREIGN KEY (user_id) REFERENCES en_user(id),
    FOREIGN KEY (product_id) REFERENCES en_product(id),
    FOREIGN KEY (activity_id) REFERENCES en_activity(id)
);
INSERT INTO en_carbon_footprint (user_id, date_calculated, carbon_footprint, product_id, activity_id) VALUES
('1', '2023-10-01 10:00:00', 12.50, 1, 1),
('2', '2023-10-02 11:00:00', 8.75, 2, 2),
('3', '2023-10-03 12:00:00', 15.20, 3, 3),
('4', '2023-10-04 13:00:00', 9.80, 4, 4),
('5', '2023-10-05 14:00:00', 11.30, 5, 5),
('6', '2023-10-06 15:00:00', 7.45, 6, 6);
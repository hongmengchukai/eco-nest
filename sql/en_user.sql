CREATE TABLE en_user (
    -- 主键，自增
    id INT AUTO_INCREMENT PRIMARY KEY,

    -- 用户账号，不为空且唯一
    account_number VARCHAR(255) NOT NULL UNIQUE,

    -- 用户密码，不为空
    password VARCHAR(255) NOT NULL,

    -- 用户名（实名）
    name VARCHAR(50),

    -- 用户邮箱，唯一
    email VARCHAR(100),

    -- 用户昵称
    nickname VARCHAR(255),

    -- 用户手机号，唯一
    phone VARCHAR(20),

    -- 用户头像
    avatar VARCHAR(255),

    -- 用户地址
    address VARCHAR(255),

    -- 登录时间
    sign_in_time DATETIME,

    -- 总共碳积分，默认为 0.0
    total_carbon DECIMAL(10, 2) DEFAULT 0.0,

    -- 账号状态，1为正常，2为封禁，不为空
    status INT NOT NULL DEFAULT 1,

    -- 创建时间，自动设置为当前时间戳
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO en_user (account_number, password, name, email, nickname, phone, avatar, address, sign_in_time, total_carbon, status)
VALUES
('user001', 'password1', '张三', 'zhangsan@example.com', '张三丰', '13800000001', 'avatar1.jpg', '北京市海淀区', '2023-10-01 08:00:00', 0.0, 1),
('user002', 'password2', '李四', 'lisi@example.com', '李四爷', '13800000002', 'avatar2.jpg', '上海市浦东新区', '2023-10-02 09:00:00', 0.0, 1),
('user003', 'password3', '王五', 'wangwu@example.com', '王五哥', '13800000003', 'avatar3.jpg', '广州市天河区', '2023-10-03 10:00:00', 0.0, 1),
('user004', 'password4', '赵六', 'zhaoliu@example.com', '赵六弟', '13800000004', 'avatar4.jpg', '深圳市南山区', '2023-10-04 11:00:00', 0.0, 1),
('user005', 'password5', '孙七', 'sunqi@example.com', '孙七郎', '13800000005', 'avatar5.jpg', '成都市武侯区', '2023-10-05 12:00:00', 0.0, 1),
('user006', 'password6', '周八', 'zhouba@example.com', '周八爷', '13800000006', 'avatar6.jpg', '杭州市西湖区', '2023-10-06 13:00:00', 0.0, 1),
('user007', 'password7', '吴九', 'wujiu@example.com', '吴九哥', '13800000007', 'avatar7.jpg', '南京市玄武区', '2023-10-07 14:00:00', 0.0, 1),
('user008', 'password8', '郑十', 'zhengshi@example.com', '郑十弟', '13800000008', 'avatar8.jpg', '武汉市江汉区', '2023-10-08 15:00:00', 0.0, 1),
('user009', 'password9', '钱十一', 'qianshiyi@example.com', '钱十一郎', '13800000009', 'avatar9.jpg', '重庆市渝中区', '2023-10-09 16:00:00', 0.0, 1),
('user010', 'password10', '孙十二', 'sunsishi@example.com', '孙十二爷', '13800000010', 'avatar10.jpg', '天津市和平区', '2023-10-10 17:00:00', 0.0, 1);

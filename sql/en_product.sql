CREATE TABLE en_product (
    -- 主键，自增
    id INT AUTO_INCREMENT PRIMARY KEY,

    -- 用户ID，外键关联en_user表的主键
    user_id INT NOT NULL,

    -- 商品名称，不能为空
    name VARCHAR(100) NOT NULL,

    -- 商品描述
    description TEXT,

    -- 商品类别
    category VARCHAR(50),

    -- 商品价格，不能为空
    price DECIMAL(10, 2) NOT NULL,

    -- 每个商品交易之后可以获得的碳值，不能为空，默认为0
    carbon_credits DECIMAL(10, 2) NOT NULL DEFAULT 0.00,

    -- 库存数量，不能为空，默认为1
    stock_quantity INT NOT NULL DEFAULT 1,

    -- 商品图片URL
    image_url VARCHAR(255),

    -- 商品图片URL集合，以逗号分隔的字符串
    image_urls TEXT,

    -- 发货地区
    shipping_region VARCHAR(255),

    -- 商品状态，商品上架或下架状态，默认为上架
    status ENUM('active', 'inactive') DEFAULT 'active',

    -- 上架时间，不能为空
    listed_at TIMESTAMP NOT NULL,

    -- 外键约束，确保user_id存在于en_user表中
    FOREIGN KEY (user_id) REFERENCES en_user(id) ON DELETE CASCADE
);

INSERT INTO en_product (user_id, name, description, category, price, carbon_credits, stock_quantity, image_url, image_urls, shipping_region, listed_at)
VALUES
(1, '高清望远镜', '这款高清望远镜具有10倍光学变焦，适合户外观察和天文观测。', '数码科技', 199.99, 10.00, 100, 'telescope.jpg', 'telescope.jpg,telescope_side.jpg', '全国', '2023-10-01 08:00:00'),
(2, '机械键盘', '这款机械键盘采用樱桃轴，手感舒适，适合长时间使用。', '数码科技', 149.99, 8.00, 50, 'keyboard.jpg', 'keyboard.jpg,keyboard_top.jpg', '北京', '2023-10-02 09:00:00'),
(3, '多功能电饭煲', '这款电饭煲具有多种烹饪模式，操作简单，适合家庭使用。', '生活用品', 99.99, 5.00, 150, 'rice_cooker.jpg', 'rice_cooker.jpg,rice_cooker_open.jpg', '上海', '2023-10-03 10:00:00'),
(4, '进口巧克力', '这款进口巧克力口感细腻，甜而不腻，是送礼佳品。', '生活用品', 29.99, 2.00, 200, 'chocolate.jpg', 'chocolate.jpg,chocolate_wrap.jpg', '广州', '2023-10-04 11:00:00'),
(5, '经典小说集', '这套经典小说集包括《三国演义》、《水浒传》等，适合文学爱好者阅读。', '图书笔记', 79.99, 3.00, 100, 'novel_set.jpg', 'novel_set.jpg,novel_set_open.jpg', '深圳', '2023-10-05 12:00:00'),
(6, '运动跑鞋', '这款运动跑鞋采用轻质材料，适合跑步和日常穿着。', '运动相关', 129.99, 6.00, 80, 'running_shoes.jpg', 'running_shoes.jpg,running_shoes_side.jpg', '杭州', '2023-10-06 13:00:00'),
(7, '智能手环', '这款智能手环具有心率监测、睡眠监测等功能，适合健康生活。', '数码科技', 79.99, 4.00, 120, 'smart_band.jpg', 'smart_band.jpg,smart_band_display.jpg', '成都', '2023-10-07 14:00:00'),
(8, '便携式榨汁机', '这款便携式榨汁机小巧轻便，适合办公室和旅行使用。', '生活用品', 59.99, 3.00, 100, 'juicer.jpg', 'juicer.jpg,juicer_open.jpg', '重庆', '2023-10-08 15:00:00'),
(9, '进口红酒', '这款进口红酒来自法国波尔多，口感醇厚，适合搭配牛排。', '生活用品', 149.99, 5.00, 50, 'red_wine.jpg', 'red_wine.jpg,red_wine_bottle.jpg', '南京', '2023-10-09 16:00:00'),
(10, '时尚太阳镜', '这款时尚太阳镜采用防紫外线镜片，适合夏季佩戴。', '生活用品', 89.99, 4.00, 70, 'sunglasses.jpg', 'sunglasses.jpg,sunglasses_side.jpg', '武汉', '2023-10-10 17:00:00');
CREATE TABLE en_comment (
    -- 评论ID，主键，自增
    id INT PRIMARY KEY AUTO_INCREMENT,

    -- 用户账号，外键，引用 en_user 表的 account_number
    user_id INT NOT NULL,

    -- 商品ID，外键，引用 en_product 表的 product_ProductID
    product_id INT NOT NULL,

    -- 评论内容
    comment VARCHAR(255) NOT NULL,

    -- 评论时间
    comment_time  DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ,

    -- 被评论的用户账号，外键，引用 en_user 表的 id
    to_user_id INT ,

    -- 被评论的评论ID，可为空或等于该表的主键
    to_comment INT,

    FOREIGN KEY (user_id) REFERENCES en_user(id),
    FOREIGN KEY (product_id) REFERENCES en_product(id),
    FOREIGN KEY (to_user_id) REFERENCES en_user(id),
    FOREIGN KEY (to_comment) REFERENCES en_comment(id)
);
INSERT INTO en_comment (user_id, product_id, comment, comment_time, to_user_id, to_comment) VALUES
('1', 1, '这个商品质量很好！', '2023-10-01 10:00:00', NULL, NULL),
('2', 2, '物流速度很快，赞一个！', '2023-10-02 11:00:00', NULL, NULL),
('3', 1, '我也觉得不错，推荐购买！', '2023-10-03 12:00:00', '1', 1),
('4', 3, '这个商品有点小瑕疵。', '2023-10-04 13:00:00', NULL, NULL),
('5', 3, '我也有同样的问题，希望能改进。', '2023-10-05 14:00:00', '2', 4),
('6', 2, '物流确实很快，服务也很好！', '2023-10-06 15:00:00', '3', 2);
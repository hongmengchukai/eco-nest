CREATE TABLE en_favorite
(
    -- 收藏物品id，主键自增
    id INT AUTO_INCREMENT PRIMARY KEY,
    -- 用户账号，绑定en_user表中user_id
    user_account_number CHAR(255) NOT NULL,
    -- 商品id，绑定en_product表中product_id
    product_id  INT NOT NULL,
    -- 创建时间，自动设置为当前时间戳
    added_at    TIMESTAMP DEFAULT CURRENT_TIMESTAMP,

    FOREIGN KEY (user_account_number) REFERENCES en_user (account_number),
    FOREIGN KEY (product_id) REFERENCES en_product (id)
);

INSERT INTO en_favorite (user_account_number, product_id)
VALUES
('user001', 1),
('user002', 2),
('user003', 3),
('user004', 4),
('user005', 5),
('user006', 6),
('user007', 7),
('user008', 8),
('user009', 9),
('user010', 10);

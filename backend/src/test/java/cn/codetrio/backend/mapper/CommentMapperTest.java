package cn.codetrio.backend.mapper;

import cn.codetrio.backend.entity.Comment;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author 梁旭
 * @version 1.0
 * @since 2024-10-15
 */
@SpringBootTest
public class CommentMapperTest {
    @Autowired
    CommentMapper commentMapper;
    //查找所有评论
    @Test
    public void findAll()
    {
        commentMapper.selectList(null).forEach(System.out::println);
    }
    //根据评论id查找评论
    @Test
    public void findById()
    {
        System.out.println(commentMapper.selectById(1));
    }
    //根据评论id删除评论

    @Test
    public void deleteByAccountNumber() {
        String accountNumber = "user006";

        // 创建 QueryWrapper 并设置删除条件
        QueryWrapper<Comment> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_account_number", accountNumber);

        // 执行删除操作
        int result = commentMapper.delete(queryWrapper);
        if (result > 0) {
            System.out.println("删除成功");
        } else {
            System.out.println("删除失败");
        }

    }

    //添加评论
    @Test
    public void addComment()
    {
        Comment comment = new Comment();
        comment.setUserAccountNumber("user007");
        comment.setProductId(1);
        comment.setComment("测试评论");
        comment.setToAccountNumber("user001");
        comment.setToComment(1);
        commentMapper.insert(comment);
    }

}

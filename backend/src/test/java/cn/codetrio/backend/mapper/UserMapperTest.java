package cn.codetrio.backend.mapper;

import cn.codetrio.backend.entity.User;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author 陈亮
 * @version 1.0
 * @since 2024-10-14
*/

// SpringBoot 集成测试
@SpringBootTest
public class UserMapperTest {
    // 自动装配 Bean
    @Autowired
    private UserMapper userMapper;

    // 查找所有用户
    @Test
    public void findAllUsersTest() {
        List<User> users = userMapper.selectList(null);
        for (User user : users){
            System.out.println(user.toString());
        }
    }

    // 姓名模糊查询
    @Test
    public void findUserByNameLikeTest() {
        // MyBatis-Plus 库中的一个类，用于构建动态 SQL 查询条件
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("nickname", "弟");

        List<User> users = userMapper.selectList(queryWrapper);
        for (User user : users){
            System.out.println(user.toString());
        }
    }
}

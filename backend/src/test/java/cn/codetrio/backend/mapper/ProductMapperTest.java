package cn.codetrio.backend.mapper;

import cn.codetrio.backend.entity.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author 陈亮
 * @version 1.0
 * @since 2024-10-14
*/

// SpringBoot 集成测试
@SpringBootTest
public class ProductMapperTest {
    // 自动装配 Bean
    @Autowired
    private ProductMapper productMapper;

    // 查找所有商品
    @Test
    public void findAllProductsTest() {
        List<Product> products = productMapper.selectList(null);
        for (Product product : products){
            System.out.println(product.toString());
        }
    }
}

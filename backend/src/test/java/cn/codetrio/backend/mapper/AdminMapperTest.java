package cn.codetrio.backend.mapper;

import cn.codetrio.backend.entity.Admin;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author 陈亮
 * @version 1.0
 * @since 2024-10-14
*/

// SpringBoot 集成测试
@SpringBootTest
public class AdminMapperTest {
    // 自动装配 Bean
    @Autowired
    private AdminMapper adminMapper;

    // 查找所有管理员
    @Test
    public void findAllAdminsTest() {
        List<Admin> admins = adminMapper.selectList(null);
        for (Admin admin : admins){
            System.out.println(admin.toString());
        }
    }

    // 添加管理员
    @Test
    public void addAdminTest() {
        // 创建 Admin 对象
        Admin admin = new Admin();
        admin.setAccountNumber("cs_admin");
        admin.setPassword("123456");
        admin.setName("测试管理员");

        // 执行插入操作
        int result = adminMapper.insert(admin);
        System.out.println("添加管理员成功，受影响行数：" + result);
    }

    // 根据账号修改管理员信息
    @Test
    public void updateAdminByAccountNumberTest() {
        String accountNumber = "cs_admin";
        String newPassword = "654321";

        // 创建 QueryWrapper 并设置修改条件
        QueryWrapper<Admin> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("account_number", accountNumber);

        // 创建 Admin 对象并设置新的密码
        Admin admin = new Admin();
        admin.setPassword(newPassword);

        // 执行修改操作
        int result = adminMapper.update(admin, queryWrapper);
        if (result > 0) {
            System.out.println("修改成功");
        } else {
            System.out.println("修改失败");
        }
    }

    // 根据账号删除管理员
    @Test
    public void deleteAdminByAccountNumberTest() {
        String accountNumber = "cs_admin";

        // 创建 QueryWrapper 并设置删除条件
        QueryWrapper<Admin> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("account_number", accountNumber);

        // 执行删除操作
        int result = adminMapper.delete(queryWrapper);
        if (result > 0) {
            System.out.println("删除成功");
        } else {
            System.out.println("删除失败");
        }
    }
}

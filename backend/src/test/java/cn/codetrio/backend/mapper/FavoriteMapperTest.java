package cn.codetrio.backend.mapper;

import cn.codetrio.backend.entity.Favorite;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author CokiLee
 * @version 1.0
 * @since 2024-10-15
 */

@SpringBootTest
public class FavoriteMapperTest {
    @Autowired
    private FavoriteMapper favoriteMapper;

    //测试查找所有收藏商品功能
    @Test
    public void findAllFavorites(){
        List<Favorite> favorites =favoriteMapper.selectList(null);
        for(Favorite favorite :favorites){
            System.out.println(favorite);
        }
    }


}

package cn.codetrio.backend.mapper;

import cn.codetrio.backend.entity.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author CokiLee
 * @version 1.0
 * @since 2024-10-15
 */

@SpringBootTest
public class OrderMapperTest {
    @Autowired
    private OrderMapper orderMapper;

    // 查找所有订单
    @Test
    public void findAllOrders() {
        List<Order> orders = orderMapper.selectList(null);
        for (Order order : orders) {
            System.out.println(order);
        }
    }

}

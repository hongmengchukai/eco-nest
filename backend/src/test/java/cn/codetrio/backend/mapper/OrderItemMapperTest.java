package cn.codetrio.backend.mapper;

import cn.codetrio.backend.entity.OrderItem;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author CokiLee
 * @version 1.0
 * @since 2024-10-15
 */

@SpringBootTest
public class OrderItemMapperTest {
    @Autowired
    private OrderItemMapper orderItemMapper;

    // 查找所有订单项目
    @Test
    public void findAllOrderItems() {
        List<OrderItem> orderItems = orderItemMapper.selectList(null);
        assertNotNull(orderItems, "Order items list should not be null");
        orderItems.forEach(orderItem -> System.out.println(orderItem));
    }
}

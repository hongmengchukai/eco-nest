package cn.codetrio.backend.mapper;

import cn.codetrio.backend.entity.Activity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
/**
 * @author 梁旭
 * @version 1.0
 * @since 2024-10-14
 */

@SpringBootTest
public class ActivityMapperTest {
    @Autowired
    ActivityMapper activityMapper;

    // 查找所有活动
    @Test
    public void findAllActivity() {
        List<Activity> activities = activityMapper.selectList(null);
        for (Activity activity : activities){
            System.out.println(activity);
        }
    }
}

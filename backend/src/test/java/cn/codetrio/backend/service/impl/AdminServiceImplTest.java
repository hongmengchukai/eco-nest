package cn.codetrio.backend.service.impl;

import cn.codetrio.backend.entity.Admin;
import cn.codetrio.backend.entity.User;
import cn.codetrio.backend.service.AdminService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author 陈亮
 * @version 1.0
 * @since 2024-10-16
*/

@SpringBootTest
public class AdminServiceImplTest {

    // 注入
    @Autowired
    private AdminService adminService;

    // 输出所有管理员信息
    @Test
    public void getAll() {
        List<Admin> list = adminService.list();
        System.out.println(list);
    }

    // 根据账号获取管理员信息
    @Test
    public void getAdminByAccountNumber() {
        QueryWrapper<Admin> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("account_number", "admin001");

        Admin admin = adminService.getOne(queryWrapper);
        System.out.println(admin);
    }
}

package cn.codetrio.backend.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author 梁旭
 * @version 1.0
 * @since 2024-10-14
 */

// 碳足迹类，对应数据库中的 en_carbon_footprint 表
@Data
@TableName("en_carbon_footprint")
public class CarbonFootprint implements Serializable {
    // 碳足迹ID，主键，自增
    @TableId(type = IdType.AUTO)
    private Integer id;

    // 用户账号，外键，引用 en_users 表的 id，不为空
    private Integer userId;

    // 计算日期，不为空
    private LocalDateTime dateCalculated;

    // 碳足迹值（单位：kg CO2e），不为空
    private double carbonFootprint;

    // 购买的商品ID，外键，引用 en_products 表的 en_productID，不为空
    private int productId;

    // 参与活动ID，外键，引用 en_activity 表的 activity_ActivityID，不为空
    private int activityId;

    // 序列化版本号
    @Serial
    private static final long serialVersionUID = 1L;
}
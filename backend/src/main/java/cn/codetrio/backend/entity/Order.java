package cn.codetrio.backend.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author CokiLee
 * @version 1.0
 * @since 2024-10-14
 */

// 用户订单项目，提供订单状态和支付状态显示
@Data
@TableName("en_order")
public class Order implements Serializable {
    // 主键，自增
    @TableId(type = IdType.AUTO)
    private Integer id;

    // 用户账号
    private String userAccountNumber;

    // 订单编号
    private String orderNumber;

    // 订单状态 (pending, completed, canceled)
    private String status;

    // 支付状态 (未支付, 已支付)
    private String paymentStatus;

    // 支付时间
    private LocalDateTime paymentTime;

    // 支付方式 (支付宝, 微信, 银行卡)
    private String paymentMethod;

    // 订单总价
    private Double totalPrice;

    // 创建时间，自动填充
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createdAt;

    // 订单项目的集合，代表该订单中的多个商品
    @TableField(exist = false)
    private List<OrderItem> orderItems;

    // 序列化版本号
    @Serial
    private static final long serialVersionUID = 1L;
}

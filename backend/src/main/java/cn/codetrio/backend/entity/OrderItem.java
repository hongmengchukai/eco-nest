package cn.codetrio.backend.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author CokiLee
 * @version 1.0
 * @since 2024-10-14
 */

// 订单详细类，记录每个订单的商品信息
@Data
@TableName("en_order_item")
public class OrderItem implements Serializable {
    // 主键，自增
    @TableId(type = IdType.AUTO)
    private Integer id;

    // 订单id，外键，关联到 en_order 表
    private Integer orderId;

    // 商品id，外键，关联到 en_product 表
    private Integer productId;

    // 商品数量，不为空
    private Integer quantity;

    // 商品单价，不为空
    private Double price;

    // 每项订单的总金额 (数量 * 单价)
    private Double totalPrice; // 可以根据数量和单价计算得出

    // 序列化版本号
    @Serial
    private static final long serialVersionUID = 1L;

    // 创建时间，记录每个订单项的创建时间
    private LocalDateTime createdAt; // 可选：记录创建时间
}

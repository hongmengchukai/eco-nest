package cn.codetrio.backend.entity;


import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author CokiLee
 * @version 1.0
 * @since 2024-10-14
 */

//用户收藏类，记录用户收藏的商品id和收藏时间
//Lombok注解，用于生成getter and setter方法
@Data
@TableName("en_favorite")
public class Favorite implements Serializable {
    //主键，自增
    @TableId(type = IdType.AUTO)
    private Integer id;

    //用户账号
    private String userAccountNumber;

    //收藏商品id
    private String productId;

    //收藏时间
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime addedAt;

    // 序列化版本号
    @Serial
    private static final long serialVersionUID = 1L;
}

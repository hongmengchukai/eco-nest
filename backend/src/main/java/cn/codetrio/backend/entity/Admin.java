package cn.codetrio.backend.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serial;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 陈亮
 * @version 1.0
 * @since 2024-10-14
*/

// 管理员类，对应数据库中的 en_admin 表
// Lombok注解: @Data，用于生成getter and setter方法
@Data
@TableName("en_admin")
public class Admin implements Serializable {
    // 主键，自增
    @TableId(type = IdType.AUTO)
    private Integer id;

    // 管理员账号，唯一
    @TableField("account_number") // 指定列名
    private String accountNumber;

    // 管理员密码
    private String password;

    // 管理员姓名
    private String name;

    // 账号创建时间，自动填充
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createdAt;

    // 当你一个类实现了Serializable接口，就会有显式地定义serialVersionUID
    // 序列化时为了保持版本的兼容性，即在版本升级时反序列化仍保持对象的唯一性。
    @Serial
    private static final long serialVersionUID = 1L;
}

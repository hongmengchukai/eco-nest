package cn.codetrio.backend.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author 梁旭
 * @version 1.0
 * @since 2024-10-14
 */

// 评论类，对应数据库中的 en_comment 表
@Data
@TableName("en_comment")
public class Comment implements Serializable {
    // 评论ID，主键，自增
    @TableId(type = IdType.AUTO)
    private Integer id;

    // 用户账号，外键，引用 en_user 表的 id
    private Integer userId;

    // 商品ID，外键，引用 en_product 表的 product_ProductID
    private int productId;

    // 评论内容
    private String comment;

    // 被评论的用户账号，外键，引用 en_user 表的 id
    private Integer toUserId;

    // 被评论的评论ID，可为空或等于该表的主键
    private Integer toComment;

    // 评论时间,不为空
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime commentTime;

    // 序列化版本号
    @Serial
    private static final long serialVersionUID = 1L;
}
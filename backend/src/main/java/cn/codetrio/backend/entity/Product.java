package cn.codetrio.backend.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serial;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author 陈亮
 * @version 1.0
 * @since 2024-10-14
*/

// 商品表，对应数据库中的 en_product 表
// Lombok注解: @Data，用于生成getter and setter方法
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("en_product")
public class Product implements Serializable {
    // 主键，自增
    @TableId(type = IdType.AUTO)
    private Integer id;

    // 用户ID，外键关联 en_user 表的主键
    @TableField("user_id")
    private Integer userId;

    // 商品名称，不能为空
    private String name;

    // 商品描述
    private String description;

    // 商品类别
    private String category;

    // 商品价格，不能为空
    private BigDecimal price;

    // 每个商品交易之后可以获得的碳值，不能为空，默认为0
    @TableField("carbon_credits")
    private BigDecimal carbonCredits = BigDecimal.ZERO;

    // 商品库存数量，不能为空，默认为1
    private Integer stockQuantity = 1;

    // 商品图片 URL
    private String imageUrl;

    // 商品图片 URL 集合，以逗号分隔的字符串
    private String imageUrls;

    // 发货地区
    private String shippingRegion;

    // 商品状态，默认为 active（上架）
    @TableField("status")
    private String status ;

    // 商品上架时间，不能为空
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime listedAt;

    // 序列化版本号
    @Serial
    private static final long serialVersionUID = 1L;
}

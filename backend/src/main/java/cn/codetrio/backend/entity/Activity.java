package cn.codetrio.backend.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author 梁旭
 * @version 1.0
 * @since 2024-10-14
 */

// 活动类，对应数据库中的 en_activity 表
@Data
@TableName("en_activity")
public class Activity implements Serializable {

    // 活动ID，主键，自增
    @TableId(type = IdType.AUTO)
    private Integer id;

    // 活动标题
    private String title;

    // 活动描述
    private String description;

    // 活动碳值
    private BigDecimal carbonValue;

    // 活动开始日期
    private LocalDateTime startDate;

    // 活动结束日期
    private LocalDateTime endDate;

    // 活动地点
    private String location;

    // 序列化版本号
    @Serial
    private static final long serialVersionUID = 1L;
}
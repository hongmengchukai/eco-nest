package cn.codetrio.backend.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 陈亮
 * @version 1.0
 * @since 2024-10-14
*/

// 用户类，对应数据库中的 en_user 表
// Lombok注解: @Data，用于生成getter and setter方法
@Data
@TableName("en_user")
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
    // 主键，自增
    @TableId(type = IdType.AUTO)
    private Integer id;

    // 用户账号，唯一
    @TableField("account_number")
    private String accountNumber; // 使用驼峰命名法

    // 用户密码，不为空
    private String password;

    // 用户名（实名）
    private String name;

    // 用户邮箱，唯一
    private String email;

    // 用户昵称
    private String nickname;

    // 用户手机号
    private String phone;

    // 用户头像
    private String avatar;

    // 用户地址
    private String address;

    // 登录时间
    private LocalDateTime signInTime;

    // 用户总碳积分，默认为 0.0
    private BigDecimal totalCarbon;

    // 账号状态，1为正常，2为封禁，不为空，默认值为 1
    private Integer status;

    // 账号创建时间，自动填充
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createdAt;

    // 序列化版本号
    @Serial
    private static final long serialVersionUID = 1L;
}

package cn.codetrio.backend.controller;

import cn.codetrio.backend.service.FileService;
import cn.codetrio.backend.util.ErrorMsg;
import cn.codetrio.backend.util.IdFactoryUtil;
import cn.codetrio.backend.util.Result;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * 文件上传控制层
 *
 * @author 陈亮
 * @version 1.0
 * @since 2024-10-21
*/

@RestController
public class FileController {

    // 获取文件路径
    @Value("${userFilePath}")
    private String userFilePath;

    // 获取文件访问路径
    @Value("${baseUrl}")
    private String baseUrl;

    // 注入service
    private final FileService fileService;
    @Autowired
    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    // 上传文件
    @PostMapping("/file")
    public Result<String> uploadFile(@RequestParam("file") MultipartFile multipartFile) {
        String uuid="file"+ IdFactoryUtil.getFileId();
        String fileName= uuid+ multipartFile.getOriginalFilename();
        try {
            if (fileService.uploadFile(multipartFile,fileName)) {
                return Result.success(baseUrl+"/image?imageName="+fileName);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return Result.fail(ErrorMsg.SYSTEM_ERROR);
        }
        return Result.fail(ErrorMsg.FILE_UPLOAD_ERROR);
    }

    // 获取图片
    @GetMapping("/image")
    public void getImage(@RequestParam("imageName") String imageName,
                         HttpServletResponse response) throws IOException {
        File fileDir = new File(userFilePath);
        File image=new File(fileDir.getAbsolutePath() +"/"+imageName);
        if (image.exists()){
            FileInputStream fileInputStream=new FileInputStream(image);
            byte[] bytes=new byte[fileInputStream.available()];
            if (fileInputStream.read(bytes)>0){
                OutputStream outputStream=response.getOutputStream();
                outputStream.write(bytes);
                outputStream.close();
            }
            fileInputStream.close();
        }
    }
}

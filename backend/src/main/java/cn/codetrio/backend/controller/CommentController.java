package cn.codetrio.backend.controller;

import cn.codetrio.backend.entity.Comment;
import cn.codetrio.backend.service.CommentService;
import cn.codetrio.backend.util.ErrorMsg;
import cn.codetrio.backend.util.Result;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author 梁旭
 * @version 1.0
 * @since 2024-10-17
 */
@RestController
@RequestMapping("/comment")
public class CommentController {
    private final CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    /**
     * 发送评论
     * @param shUserId 用户的唯一标识符，从Cookie中获取
     * @param comment 评论内容，从请求体中获取
     * @return 操作结果
     */
    @PostMapping("/send")
    public Result<Void> sendComment(
            @CookieValue("shUserId")
            @NotNull(message = "登录异常 请重新登录")
            @NotEmpty(message = "登录异常 请重新登录") String shUserId,
            @RequestBody Comment comment) {

        comment.setUserId(Integer.valueOf(shUserId));
        comment.setCommentTime(LocalDateTime.now());
        boolean success = commentService.save(comment);
        return success ? Result.success() : Result.fail(ErrorMsg.COMMIT_FAIL_ERROR);
    }

    /**
     * 获取单条评论信息
     * @param id 评论的唯一标识符
     * @return 评论信息
     */
    @GetMapping("/info")
    public Result<Comment> getComment(@RequestParam Integer id) {
        Comment comment = commentService.getById(id);
        return comment != null ? Result.success(comment) : Result.fail(ErrorMsg.FILE_NOT_EXIT);
    }

    /**
     * 获取某个产品的所有评论
     * @param productId 产品的唯一标识符
     * @return 该产品的所有评论
     */
    @GetMapping("/product")
    public Result<List<Comment>> getAllProductComments(@RequestParam Integer productId) {
        QueryWrapper<Comment> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("product_id", productId);
        List<Comment> comments = commentService.list(queryWrapper);
        return Result.success(comments);
    }

    /**
     * 获取当前用户的所有评论
     * @param shUserId 用户的唯一标识符，从Cookie中获取
     * @return 该用户的所有评论
     */
    @GetMapping("/my")
    public Result<List<Comment>> getAllMyComments(
            @CookieValue("shUserId")
            @NotNull(message = "登录异常 请重新登录")
            @NotEmpty(message = "登录异常 请重新登录") String shUserId) {

        QueryWrapper<Comment> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_account_number", shUserId);
        List<Comment> comments = commentService.list(queryWrapper);
        return Result.success(comments);
    }

    /**
     * 删除评论
     * @param shUserId 用户的唯一标识符，从Cookie中获取
     * @param id 评论的唯一标识符
     * @return 操作结果
     */
    @GetMapping("/delete")
    public Result<Void> deleteComment(@CookieValue("shUserId")
                                          @NotNull(message = "登录异常 请重新登录")
                                          @NotEmpty(message = "登录异常 请重新登录") String shUserId,
                                      @RequestParam Long id) {
        boolean success = commentService.removeById(id);
        return success ? Result.success() : Result.fail(ErrorMsg.SYSTEM_ERROR);
    }
}
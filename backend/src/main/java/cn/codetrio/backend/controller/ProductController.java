package cn.codetrio.backend.controller;

import cn.codetrio.backend.entity.Product;
import cn.codetrio.backend.service.ProductService;
import cn.codetrio.backend.util.ErrorMsg;
import cn.codetrio.backend.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 商品控制器
 *
 * @author 陈亮
 * @version 1.0
 * @since 2024-10-20
*/

@RestController  // 标记类为一个 RESTful 控制器
@RequestMapping("/product")  // 定义请求路径
public class ProductController {

    // 注入商品服务
    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    // 添加商品
    @PostMapping("/add")
    public Result<Product> addIdleItem(@CookieValue("shUserId")
                                    @NotNull(message = "登录异常 请重新登录")
                                    @NotEmpty(message = "登录异常 请重新登录") String shUserId,
                              @RequestBody Product product){
        product.setUserId(Integer.valueOf(shUserId));
        product.setImageUrl("");
        product.setStockQuantity(1);
        product.setStatus("active");
        if(productService.addProduct(product)){
            return Result.success(product);
        }
        return Result.fail(ErrorMsg.SYSTEM_ERROR);
    }

    // 获取商品信息
    @GetMapping("/info")
    public Result<Product> getProduct(@RequestParam Integer id){
        Product product = productService.getProduct(id);
        if (product != null) {
            return Result.success(product);
        } else {
            return Result.fail(ErrorMsg.NOT_FOUND);
        }
    }
}

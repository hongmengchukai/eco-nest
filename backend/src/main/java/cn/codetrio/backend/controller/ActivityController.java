package cn.codetrio.backend.controller;

import cn.codetrio.backend.entity.Activity;
import cn.codetrio.backend.service.ActivityService;
import cn.codetrio.backend.util.ErrorMsg;
import cn.codetrio.backend.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/activity")
public class ActivityController {

    @Autowired
    private ActivityService activityService;

    @GetMapping("/info")
    public Result<List<Activity>> getAllActivities() {
        List<Activity> activities = activityService.getAll();
        if (!activities.isEmpty()) {
            return Result.success(activities);
        } else {
            return Result.fail(ErrorMsg.NOT_FOUND);
        }
    }

    @PostMapping("/save")
    public Result<String> saveActivity(@RequestBody Activity activity) {
        boolean success = activityService.save(activity);
        if (success) {
            return Result.success("Activity saved successfully");
        } else {
            return Result.fail(ErrorMsg.COMMIT_FAIL_ERROR);
        }
    }

    @PutMapping("/update")
    public Result<String> updateActivity(@RequestBody Activity activity) {
        boolean success = activityService.update(activity);
        if (success) {
            return Result.success("Activity updated successfully");
        } else {
            return Result.fail(ErrorMsg.COMMIT_FAIL_ERROR);
        }
    }

    @DeleteMapping("/delete")
    public Result<String> deleteActivity(@RequestParam Integer id) {
        boolean success = activityService.deleteById(id);
        if (success) {
            return Result.success("Activity deleted successfully");
        } else {
            return Result.fail(ErrorMsg.COMMIT_FAIL_ERROR);
        }
    }
}
package cn.codetrio.backend.controller;

import cn.codetrio.backend.entity.Admin;
import cn.codetrio.backend.entity.Product;
import cn.codetrio.backend.entity.User;
import cn.codetrio.backend.service.AdminService;
import cn.codetrio.backend.service.OrderService;
import cn.codetrio.backend.service.ProductService;
import cn.codetrio.backend.service.UserService;
import cn.codetrio.backend.util.ErrorMsg;
import cn.codetrio.backend.util.Result;
import jakarta.servlet.http.HttpSession;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 管理员控制器
 *
 * @author 陈亮
 * @version 1.0
 * @since 2024-10-15
*/

@RestController  // 标记类为一个 RESTful 控制器
@RequestMapping("/admin")  // 定义请求路径
public class AdminController {

    // 注入管理员服务
    private final AdminService adminService;
    private final UserService userService;
    private final ProductService productService;
    private final OrderService orderService;

    @Autowired
    public AdminController(AdminService adminService, UserService userService, ProductService productService, OrderService orderService) {
        this.adminService = adminService;
        this.userService = userService;
        this.productService = productService;
        this.orderService = orderService;
    }

    // 管理员登录
    @GetMapping("/login")
    public Result<Admin> login(@RequestParam("accountNumber") @NotNull @NotEmpty String accountNumber,
                          @RequestParam("adminPassword") @NotNull @NotEmpty String adminPassword,
                          HttpSession session){

        Admin admin=adminService.login(accountNumber,adminPassword);
        if (admin == null) {
            return Result.fail(ErrorMsg.EMAIL_LOGIN_ERROR);
        }
        session.setAttribute("admin",admin);

        return Result.success(admin);
    }

    // 退出登录
    @GetMapping("/loginOut")
    public Result<Admin> loginOut( HttpSession session){
        session.removeAttribute("admin");
        return Result.success();
    }

    // 获取管理员列表
    @GetMapping("/list")
    public Result<Map<String, Object>> getAdminList(HttpSession session,
                                                    @RequestParam(value = "page",required = false) Integer page,
                                                    @RequestParam(value = "nums",required = false) Integer nums){
        if(session.getAttribute("admin")==null){
            return Result.fail(ErrorMsg.COOKIE_ERROR);
        }
        int p=1;
        int n=8;
        if(null!=page){
            p = page>0?page:1;
        }
        if(null!=nums){
            n = nums>0?nums:8;
        }

        List<Admin> adminList = adminService.getAdminList(p, n);
        int adminCount = adminService.getAdminCount(); // 获取管理员总数

        Map<String, Object> resultData = new HashMap<>();
        resultData.put("adminList", adminList);
        resultData.put("count", adminCount);

        return Result.success(resultData);
    }

    // 添加管理员
    @PostMapping("/add")
    public Result<String> addAdmin(HttpSession session,
                             @RequestBody Admin admin){
        if(session.getAttribute("admin")==null){
            return Result.fail(ErrorMsg.COOKIE_ERROR);
        }
        if(adminService.addAdmin(admin)){
            return Result.success();
        }
        return Result.fail(ErrorMsg.PARAM_ERROR);
    }

    // 获取商品列表
    @GetMapping("/productList")
    public Result<Map<String, Object>> productList(HttpSession session,
                             @RequestParam("status") @NotNull @NotEmpty String status,
                             @RequestParam(value = "page",required = false) Integer page,
                             @RequestParam(value = "nums",required = false) Integer nums){
        if(session.getAttribute("admin")==null){
            return Result.fail(ErrorMsg.COOKIE_ERROR);
        }
        int p=1;
        int n=8;
        if(null!=page){
            p=page>0?page:1;
        }
        if(null!=nums){
            n=nums>0?nums:8;
        }

        Map<String, Object> result = productService.adminGetProductList(status, p, n);
        return Result.success(result);
    }

    // 更新商品状态
    @GetMapping("/updateProductStatus")
    public Result<Void> updateProductStatus(HttpSession session,
                                     @RequestParam("id") @NotNull @NotEmpty Integer id,
                                     @RequestParam("status") @NotNull @NotEmpty String status){
        if(session.getAttribute("admin")==null){
            return Result.fail(ErrorMsg.COOKIE_ERROR);
        }
        Product product=new Product();
        product.setId(id);
        product.setStatus(status);
        if(productService.updateProduct(product)){
            return Result.success();
        }
        return Result.fail(ErrorMsg.SYSTEM_ERROR);
    }

    // 获取订单列表
    @GetMapping("/orderList")
    public Result<Map<String, Object>> orderList(HttpSession session,
                              @RequestParam(value = "page",required = false) Integer page,
                              @RequestParam(value = "nums",required = false) Integer nums){
        if(session.getAttribute("admin")==null){
            return Result.fail(ErrorMsg.COOKIE_ERROR);
        }
        int p=1;
        int n=8;
        if(null!=page){
            p=page>0?page:1;
        }
        if(null!=nums){
            n=nums>0?nums:8;
        }
        return Result.success(orderService.getAllOrder(p,n));
    }

    // 管理员删除订单
    @GetMapping("/deleteOrder")
    public Result<Void> deleteOrder(HttpSession session,
                              @RequestParam("id") @NotNull @NotEmpty Integer id){
        if(session.getAttribute("admin")==null){
            return Result.fail(ErrorMsg.COOKIE_ERROR);
        }
        if(orderService.deleteOrder(id)){
            return Result.success();
        }
        return Result.fail(ErrorMsg.SYSTEM_ERROR);
    }

    // 获取用户列表
    @GetMapping("/userList")
    public Result<Map<String, Object>> userList(HttpSession session,
                             @RequestParam(value = "page",required = false) Integer page,
                             @RequestParam(value = "nums",required = false) Integer nums,
                             @RequestParam("status") @NotNull @NotEmpty Integer status){
        if(session.getAttribute("admin")==null){
            return Result.fail(ErrorMsg.COOKIE_ERROR);
        }
        int p=1;
        int n=8;
        if(null!=page){
            p=page>0?page:1;
        }
        if(null!=nums){
            n=nums>0?nums:8;
        }
        return Result.success(userService.getUserByStatus(status,p,n));
    }

    // 更新用户状态
    @GetMapping("/updateUserStatus")
    public Result<Void> updateUserStatus(HttpSession session,
                                     @RequestParam("id") @NotNull @NotEmpty Integer id,
                                     @RequestParam("status") @NotNull @NotEmpty Integer status){
        if(session.getAttribute("admin")==null){
            return Result.fail(ErrorMsg.COOKIE_ERROR);
        }
        User user =new User();
        user.setId(id);
        user.setStatus(status);
        if(userService.updateUser(user))
            return Result.success();
        return Result.fail(ErrorMsg.SYSTEM_ERROR);
    }

    // 按商品名称查询
    @GetMapping("/queryProduct")
    public Result<Map<String, Object>> queryProduct(@RequestParam(value = "findValue",required = false) String findValue,
                              @RequestParam(value = "page",required = false) Integer page,
                              @RequestParam(value = "nums",required = false) Integer nums,
                              @RequestParam("status") @NotNull @NotEmpty String status){

        if(findValue == null){
            findValue="";
        }
        int p=1;
        int n=8;
        if(null!=page){
            p=page>0?page:1;
        }
        if(null!=nums){
            n=nums>0?nums:8;
        }

        System.out.println("---------------------" + "按照商品名称查找：" + findValue + " 商品状态：" + status+ "--------------");

        return Result.success(productService.findProduct(findValue,status,p,n));
    }

    // 按订单号查询订单
    /*@GetMapping("/queryOrder")
    public Result queryOrder(HttpSession session,
                              @RequestParam(value = "page",required = false) Integer page,
                              @RequestParam(value = "nums",required = false) Integer nums,
                              @RequestParam(value = "searchValue",required = false) String searchValue){

        if(session.getAttribute("admin")==null){
            return Result.fail(ErrorMsg.COOKIE_ERROR);
        }

        int p=1;
        int n=8;
        if(null!=page){
            p=page>0?page:1;
        }
        if(null!=nums){
            n=nums>0?nums:8;
        }

        System.out.println("---------------------" + searchValue + "--------------");

        if(null == searchValue || "".equals(searchValue))
            return Result.success(orderService.getAllOrder(p,n));
        return Result.success(orderService.findOrderByNumber(searchValue, p, n));
    }*/

    // 根据用户账号查找
    @GetMapping("/queryUser")
    public Result<Map<String, Object>> queryUser(HttpSession session,
                              @RequestParam(value = "searchValue",required = false) String searchValue,
                              @RequestParam(value = "mode",required = false) Integer mode,
                              @RequestParam(value = "page",required = false) Integer page,
                              @RequestParam(value = "nums",required = false) Integer nums){
        if(session.getAttribute("admin")==null){
            return Result.fail(ErrorMsg.COOKIE_ERROR);
        }
        int p=1;
        int n=8;
        if(null!=page){
            p=page>0?page:1;
        }
        if(null!=nums){
            n=nums>0?nums:8;
        }

        if(mode == 1){
            if(null == searchValue || searchValue.isEmpty()){
                return Result.success(userService.getUserByStatus(1,p,n));
            }else{
                return Result.success(userService.getUserByNumber(searchValue,mode));
            }
        }else if (mode == 2){
            if(null == searchValue || searchValue.isEmpty()){
                return Result.success(userService.getUserByStatus(2,p,n));
            }else{
                return Result.success(userService.getUserByNumber(searchValue,mode));
            }
        }
        return Result.fail(ErrorMsg.SYSTEM_ERROR);
    }
}

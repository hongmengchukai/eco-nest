package cn.codetrio.backend.controller;

import cn.codetrio.backend.entity.User;
import cn.codetrio.backend.service.UserService;
import cn.codetrio.backend.util.ErrorMsg;
import cn.codetrio.backend.util.Result;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * 用户控制器
 *
 * @author 陈亮
 * @version 1.0
 * @since 2024-10-19
*/

@RestController  // 标记类为一个 RESTful 控制器
@RequestMapping("/user")  // 定义请求路径
public class UserController {

    // 注入用户服务
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    // 用户注册
    @PostMapping("/sign-in")
    public Result<User> signIn(@RequestBody User user) {
        System.out.println(user);
        user.setSignInTime(LocalDateTime.now());
        if (userService.userSignIn(user)) {
            return Result.success(user);
        }
        return Result.fail(ErrorMsg.REGISTER_ERROR);
    }

    // 用户登录
    @RequestMapping("/login")
    public Result<User> login(@RequestParam("accountNumber") @NotEmpty @NotNull String accountNumber,
                          @RequestParam("userPassword") @NotEmpty @NotNull String userPassword,
                          HttpServletResponse response) {
        User user = userService.userLogin(accountNumber, userPassword);

        System.out.println("登录：" + user);

        System.out.println("===================================");
        System.out.println(accountNumber + "   " + userPassword);
        System.out.println("===================================");

        if (null == user) {
            return Result.fail(ErrorMsg.EMAIL_LOGIN_ERROR);
        }

        // 用户名或者密码为空
        if(accountNumber.isEmpty() || userPassword.isEmpty()){
            return Result.fail(ErrorMsg.EMAIL_LOGIN_ERROR);
        }

        Cookie cookie = new Cookie("shUserId", String.valueOf(user.getId()));
        cookie.setMaxAge(60*60);
        cookie.setPath("/");
        cookie.setHttpOnly(false);
        response.addCookie(cookie);
        return Result.success(user);
    }

    // 退出登录
    @RequestMapping("/logout")
    public Result<Void> logout(@CookieValue("shUserId")
                           @NotNull(message = "登录异常 请重新登录")
                           @NotEmpty(message = "登录异常 请重新登录") String shUserId, HttpServletResponse response) {
        Cookie cookie = new Cookie("shUserId", shUserId);
        cookie.setMaxAge(0);
        cookie.setPath("/");
        cookie.setHttpOnly(true);
        response.addCookie(cookie);
        return Result.success();
    }

    // 获取用户信息
    @GetMapping("/info")
    public Result<User> getOneUser(@CookieValue("shUserId") @NotNull(message = "登录异常 请重新登录")
                               @NotEmpty(message = "登录异常 请重新登录")
                                       String id) {
        return Result.success(userService.getUser(Integer.valueOf(id)));
    }

    // 修改用户信息
    @PostMapping("/info")
    public Result<User> updateUserPublicInfo(@CookieValue("shUserId") @NotNull(message = "登录异常 请重新登录")
                                     @NotEmpty(message = "登录异常 请重新登录")
                                             String id, @RequestBody  User user) {
        user.setId(Integer.valueOf(id));

        if (userService.updateUser(user)) {
            return Result.success();
        }
        return Result.fail(ErrorMsg.SYSTEM_ERROR);
    }

    // 修改用户密码
    @GetMapping("/password")
    public Result<Void> updateUserPassword(@CookieValue("shUserId") @NotNull(message = "登录异常 请重新登录")
                                       @NotEmpty(message = "登录异常 请重新登录") String id,
                                       @RequestParam("oldPassword") @NotEmpty @NotNull String oldPassword,
                                       @RequestParam("newPassword") @NotEmpty @NotNull String newPassword) {
        if (userService.updatePassword(newPassword,oldPassword,Integer.valueOf(id))) {
            return Result.success();
        }
        return Result.fail(ErrorMsg.PASSWORD_RESET_ERROR);
    }
}

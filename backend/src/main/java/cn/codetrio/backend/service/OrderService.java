package cn.codetrio.backend.service;

import cn.codetrio.backend.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Map;

/**
 * @author CokiLee、陈亮
 * @version 1.0
 * @since 2024-10-16
 */

public interface OrderService extends IService<Order> {
    // 获取订单列表
    Map<String, Object> getAllOrder(int p, int n);

    // 删除订单
    boolean deleteOrder(Integer id);
}

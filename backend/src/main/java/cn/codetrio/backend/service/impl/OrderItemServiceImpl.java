package cn.codetrio.backend.service.impl;

import cn.codetrio.backend.entity.OrderItem;
import cn.codetrio.backend.mapper.OrderItemMapper;
import cn.codetrio.backend.service.OrderItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.io.Serial;

/**
 * @author CokiLee
 * @version 1.0
 * @since 2024-10-16
 */

@Service
public class OrderItemServiceImpl extends ServiceImpl<OrderItemMapper, OrderItem> implements OrderItemService {
}

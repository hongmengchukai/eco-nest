package cn.codetrio.backend.service;

import cn.codetrio.backend.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Map;

/**
 * @author 陈亮
 * @version 1.0
 * @since 2024-10-15
*/

public interface UserService extends IService<User> {

    // 用户注册
    boolean userSignIn(User user);

    // 用户登录
    User userLogin(String accountNumber,String userPassword);

    // 获取用户信息
    User getUser(Integer id);

    // 获取正常用户列表
    Map<String, Object> getUserByStatus(Integer status, int p, int n);

    // 更新用户信息
    boolean updateUser(User user);

    // 修改用户密码
    boolean updatePassword(String newPassword, String oldPassword, Integer id);

    // 根据用户账号查找
    Map<String, Object> getUserByNumber(String searchValue, Integer mode);
}

package cn.codetrio.backend.service.impl;

import cn.codetrio.backend.entity.Order;
import cn.codetrio.backend.mapper.OrderMapper;
import cn.codetrio.backend.service.OrderService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author CokiLee、陈亮
 * @version 1.0
 * @since 2024-10-16
 */

@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {
    // 获取订单列表
    @Override
    public Map<String, Object> getAllOrder(int p, int n) {

        // 创建分页对象
        Page<Order> page = new Page<>(p, n);

        // 执行分页查询
        Page<Order> orderPage = page(page, new QueryWrapper<>());

        // 构建返回结果
        Map<String, Object> result = new HashMap<>();
        result.put("orderList", orderPage.getRecords());
        result.put("total", orderPage.getTotal());

        return result;
    }

    // 删除订单
    @Override
    public boolean deleteOrder(Integer id) {
        return removeById(id);
    }
}

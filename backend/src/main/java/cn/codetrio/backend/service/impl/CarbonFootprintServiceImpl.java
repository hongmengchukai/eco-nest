package cn.codetrio.backend.service.impl;

import cn.codetrio.backend.entity.CarbonFootprint;
import cn.codetrio.backend.mapper.CarbonFootprintMapper;
import cn.codetrio.backend.service.CarbonFootprintService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author 梁旭
 * @version 1.0
 * @since 2024-10-16
 */
@Service
public class CarbonFootprintServiceImpl extends ServiceImpl<CarbonFootprintMapper, CarbonFootprint> implements CarbonFootprintService {
}

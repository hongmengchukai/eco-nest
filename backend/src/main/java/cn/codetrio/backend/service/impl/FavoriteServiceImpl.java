package cn.codetrio.backend.service.impl;

import cn.codetrio.backend.entity.Favorite;
import cn.codetrio.backend.mapper.FavoriteMapper;
import cn.codetrio.backend.service.FavoriteService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author CokiLee
 * @version 1.0
 * @since 2024-10-16
 */

@Service
public class FavoriteServiceImpl extends ServiceImpl<FavoriteMapper, Favorite> implements FavoriteService {
}

package cn.codetrio.backend.service;

import cn.codetrio.backend.entity.Activity;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author 梁旭
 * @version 1.0
 * @since 2024-10-16
 */
public interface ActivityService extends IService<Activity> {
        Activity getById(Integer id);
        List<Activity> getAll();
        boolean save(Activity activity);
        boolean update(Activity activity);
        boolean deleteById(Integer id);
}

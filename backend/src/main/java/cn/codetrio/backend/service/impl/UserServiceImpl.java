package cn.codetrio.backend.service.impl;

import cn.codetrio.backend.entity.User;
import cn.codetrio.backend.mapper.UserMapper;
import cn.codetrio.backend.service.UserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 陈亮
 * @version 1.0
 * @since 2024-10-15
*/

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    // 注入 UserMapper
    private final UserMapper userMapper;

    @Autowired
    public UserServiceImpl(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    // 用户注册
    @Override
    public boolean userSignIn(User user) {
        // 检查用户账号是否已存在
        User existingUser = getOne(new QueryWrapper<User>().eq("account_number", user.getAccountNumber()));
        if (existingUser != null) {
            return false; // 用户账号已存在
        }

        // 插入新用户
        return save(user);
    }

    // 用户登录
    @Override
    public User userLogin(String accountNumber, String userPassword) {
        User user = getOne(new QueryWrapper<User>()
                .eq("account_number", accountNumber)
                .eq("password", userPassword));

        if (user != null) {
            // 更新登录时间
            LocalDateTime now = LocalDateTime.now();
            UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq("account_number", accountNumber)
                         .set("sign_in_time", now);
            // 执行更新操作
            update(updateWrapper);

            // 更新本地对象的登录时间
            user.setSignInTime(now);
        }

        return user;
    }

    // 获取用户信息
    @Override
    public User getUser(Integer id) {
        return baseMapper.selectById(id);
    }

    // 获取正常用户列表
    @Override
    public Map<String, Object> getUserByStatus(Integer status, int p, int n) {
        // 创建分页对象
        Page<User> page = new Page<>(p, n);

        // 查询条件
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", status);

        // 执行分页查询
        IPage<User> userPage = baseMapper.selectPage(page, queryWrapper);

        // 构建返回结果
        Map<String, Object> result = new HashMap<>();
        result.put("userList", userPage.getRecords());
        result.put("total", userPage.getTotal());

        return result;
    }

    // 更新用户信息
    @Override
    public boolean updateUser(User user) {
        return updateById(user);
    }

    // 修改用户密码
    @Override
    public boolean updatePassword(String newPassword, String oldPassword, Integer id) {
        // 构建更新条件
        LambdaUpdateWrapper<User> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(User::getId, id)
                        .eq(User::getPassword, oldPassword);

        // 创建新的 User 对象并设置密码
        User user = new User();
        user.setPassword(newPassword);

        // 更新新密码
        return userMapper.update(user, updateWrapper) > 0;
    }

    // 根据用户账号查找
    @Override
    public Map<String, Object> getUserByNumber(String searchValue, Integer mode) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("account_number", searchValue)
                    .eq("status", mode);

        List<User> users = userMapper.selectList(queryWrapper);
        Map<String, Object> result = new HashMap<>();
        result.put("userList", users);
        result.put("total", users.size());
        return result;
    }
}

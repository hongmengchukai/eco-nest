package cn.codetrio.backend.service.impl;

import cn.codetrio.backend.entity.Comment;
import cn.codetrio.backend.mapper.CommentMapper;
import cn.codetrio.backend.service.CommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author 梁旭
 * @version 1.0
 * @since 2024-10-16
 */
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements CommentService {
}

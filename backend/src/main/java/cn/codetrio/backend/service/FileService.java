package cn.codetrio.backend.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * 文件上传服务
 *
 * @author 陈亮
 * @version 1.0
 * @since 2024-10-21
*/

public interface FileService {

    // 上传文件
    boolean uploadFile(MultipartFile multipartFile, String fileName)throws IOException;
}

package cn.codetrio.backend.service;

import cn.codetrio.backend.entity.Product;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * @author 陈亮
 * @version 1.0
 * @since 2024-10-15
*/

public interface ProductService extends IService<Product> {

    // 管理员获取商品列表
    Map<String, Object> adminGetProductList(String status, int p, int n);

    // 更新商品
    boolean updateProduct(Product product);

    // 按商品名称查询
    Map<String, Object> findProduct(String findValue,String status, int p, int n) ;

    // 添加商品
    boolean addProduct(Product product);

    // 获取商品详情
    Product getProduct(Integer id);
}

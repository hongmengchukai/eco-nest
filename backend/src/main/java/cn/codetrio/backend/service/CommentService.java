package cn.codetrio.backend.service;

import cn.codetrio.backend.entity.Comment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author 梁旭
 * @version 1.0
 * @since 2024-10-16
 */
public interface CommentService extends IService<Comment> {
}

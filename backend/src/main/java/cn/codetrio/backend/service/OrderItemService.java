package cn.codetrio.backend.service;

import cn.codetrio.backend.entity.OrderItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author CokiLee
 * @version 1.0
 * @since 2024-10-16
 */

public interface OrderItemService extends IService<OrderItem> {

}

package cn.codetrio.backend.service.impl;

import cn.codetrio.backend.entity.Product;
import cn.codetrio.backend.mapper.ProductMapper;
import cn.codetrio.backend.service.ProductService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author 陈亮
 * @version 1.0
 * @since 2024-10-15
*/

@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements ProductService {

    // 注入 ProductMapper
    private final ProductMapper productMapper;

    @Autowired
    public ProductServiceImpl(ProductMapper productMapper) {
        this.productMapper = productMapper;
    }

    // 管理员获取商品列表
    @Override
    public Map<String, Object> adminGetProductList(String status, int p, int n) {
        Page<Product> page = new Page<>(p, n);
        QueryWrapper<Product> queryWrapper = new QueryWrapper<>();
        if (status != null) {
            queryWrapper.eq("status", status);
        }

        // 查询分页数据
        List<Product> products = baseMapper.selectPage(page, queryWrapper).getRecords();

        // 计算总商品数
        long total = baseMapper.selectCount(queryWrapper);

        // 构建返回结果
        Map<String, Object> result = new HashMap<>();
        result.put("total", total);
        result.put("productList", products.stream()
            .map(product -> {
                Map<String, Object> map = new HashMap<>();
                map.put("id", product.getId());
                map.put("name", product.getName());
                map.put("status", product.getStatus());
                map.put("description", product.getDescription());
                map.put("category", product.getCategory());
                map.put("price", product.getPrice());
                map.put("carbonCredits", product.getCarbonCredits());
                map.put("stockQuantity", product.getStockQuantity());
                map.put("listedAt", product.getListedAt());
                map.put("userId", product.getUserId());
                return map;
            })
            .collect(Collectors.toList()));

        return result;
    }

    // 更新商品
    @Override
    public boolean updateProduct(Product product) {
        return updateById(product);
    }

    // 按商品名称查询
    @Override
    public Map<String, Object> findProduct(String findValue, String status, int p, int n) {
        // 创建分页对象
        Page<Product> page = new Page<>(p, n);

        // 创建查询条件
        QueryWrapper<Product> queryWrapper = new QueryWrapper<>();
        if (findValue != null && !findValue.isEmpty()) {
            queryWrapper.like("name", findValue);
        }
        if (status != null && !status.isEmpty()) {
            queryWrapper.eq("status", status);
        }

        // 执行分页查询
        Page<Product> productPage = productMapper.selectPage(page, queryWrapper);

        // 构建返回结果
        Map<String, Object> result = new HashMap<>();
        result.put("productList", productPage.getRecords());
        result.put("total", productPage.getTotal());

        return result;
    }

    // 添加商品
    @Override
    public boolean addProduct(Product product) {
        // 使用 MyBatis-Plus 的 insert 方法添加商品
        return save(product);
    }

    // 获取商品信息
    @Override
    public Product getProduct(Integer id) {
        return productMapper.selectById(id);
    }
}

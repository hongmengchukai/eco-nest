package cn.codetrio.backend.service.impl;

import cn.codetrio.backend.service.FileService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * 文件上传服务实现类
 *
 * @author 陈亮
 * @version 1.0
 * @since 2024-10-21
*/

@Service
public class FileServiceImpl implements FileService {

    // 文件保存路径
    @Value("${userFilePath}")
    private String userFilePath;

    // 上传文件
    @Override
    public boolean uploadFile(MultipartFile multipartFile, String fileName) throws IOException {
        // 判断文件目录是否存在
        File fileDir = new File(userFilePath);
        if (!fileDir.exists()) {
            if (!fileDir.mkdirs()) {
                return false;
            }
        }

        // 打印文件的绝对路径
        System.out.println(fileDir.getAbsolutePath() +"/"+fileName);

        // 删除文件
        File file = new File(fileDir.getAbsolutePath() +"/"+fileName);
        if (file.exists()) {
            if (!file.delete()) {
                return false;
            }
        }
        if (file.createNewFile()) {
            multipartFile.transferTo(file);
            return true;
        }

        return false;
    }
}

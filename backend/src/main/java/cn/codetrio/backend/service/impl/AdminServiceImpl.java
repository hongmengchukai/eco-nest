package cn.codetrio.backend.service.impl;

import cn.codetrio.backend.entity.Admin;
import cn.codetrio.backend.mapper.AdminMapper;
import cn.codetrio.backend.service.AdminService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 陈亮
 * @version 1.0
 * @since 2024-10-15
*/

@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements AdminService {
    // 管理员登录
    @Override
    public Admin login(String accountNumber, String adminPassword) {
        LambdaQueryWrapper<Admin> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Admin::getAccountNumber, accountNumber)
                    .eq(Admin::getPassword, adminPassword);

        return getOne(queryWrapper);
    }

    // 获取管理员列表
    @Override
    public List<Admin> getAdminList(int p, int n) {
        // 创建分页对象
        IPage<Admin> page = new Page<>(p, n);

        // 执行分页查询
        IPage<Admin> resultPage = baseMapper.selectPage(page, null);

        // 返回分页结果
        return resultPage.getRecords();
    }

    // 添加管理员
    @Override
    public boolean addAdmin(Admin admin) {
        return save(admin);
    }

    // 获取管理员总数
    @Override
    public int getAdminCount() {
        QueryWrapper<Admin> queryWrapper = new QueryWrapper<>();
        return (int) this.count(queryWrapper);
    }
}

package cn.codetrio.backend.service;

import cn.codetrio.backend.entity.Admin;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author 陈亮
 * @version 1.0
 * @since 2024-10-15
*/

public interface AdminService extends IService<Admin> {
    // 管理员登录
    Admin login(String accountNumber,String adminPassword);

    // 获取管理员列表
    List<Admin> getAdminList(int p, int n);

    // 添加管理员
    boolean addAdmin(Admin admin);

    // 获取管理员总数
    int getAdminCount();
}

package cn.codetrio.backend.service;

import cn.codetrio.backend.entity.Favorite;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author CokiLee
 * @version 1.0
 * @since 2024-10-16
 */

public interface FavoriteService extends IService<Favorite> {

}

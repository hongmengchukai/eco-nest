package cn.codetrio.backend.service.impl;

import cn.codetrio.backend.entity.Activity;
import cn.codetrio.backend.mapper.ActivityMapper;
import cn.codetrio.backend.service.ActivityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivityServiceImpl extends ServiceImpl<ActivityMapper, Activity> implements ActivityService {

    @Autowired
    private ActivityMapper activityMapper;

    @Override
    public Activity getById(Integer id) {
        return activityMapper.selectById(id);
    }

    @Override
    public boolean save(Activity activity) {
        return activityMapper.insert(activity) > 0;
    }

    @Override
    public boolean update(Activity activity) {
        return activityMapper.updateById(activity) > 0;
    }

    @Override
    public boolean deleteById(Integer id) {
        return activityMapper.deleteById(id) > 0;
    }

    @Override
    public List<Activity> getAll() {
        return activityMapper.selectList(null);
    }
}
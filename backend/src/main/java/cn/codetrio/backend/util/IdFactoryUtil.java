package cn.codetrio.backend.util;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * id生成工具类
 *
 * @author 陈亮
 * @version 1.0
 * @since 2024-10-21
*/

public class IdFactoryUtil {

    // 订单id尾数
    private static final AtomicInteger orderIdEnd=new AtomicInteger(1);
    // 文件id尾数
    private static final AtomicInteger fileIdEnd=new AtomicInteger(1);

    // 生成订单id
    public static String getOrderId(){
        int newI;
        int ord;
        do{
            ord=orderIdEnd.get();
            newI=(ord+1)%10000;
        }
        while (!orderIdEnd.compareAndSet(ord,newI));
        return System.currentTimeMillis()+""+(newI+10000);
    }

    // 生成文件id
    public static String getFileId(){
        int newI;
        int ord;
        do{
            ord=fileIdEnd.get();
            newI=(ord+1)%1000;
        }
        while (!fileIdEnd.compareAndSet(ord,newI));
        return System.currentTimeMillis()+""+(newI+1000);
    }
}

package cn.codetrio.backend.util;

import java.util.Map;

/**
 * 用于在参数验证失败时抛出自定义异常，并携带详细的错误信息，方便全局异常处理器统一处理和返回。
 *
 * @author 陈亮
 * @version 1.0
 * @since 2024-10-17
 */

public class ParamException extends RuntimeException {

    private Map map;

    public ParamException(Map map) {
        this.map = map;
    }

    public Map getMap() {
        return map;
    }
}

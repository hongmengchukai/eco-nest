package cn.codetrio.backend.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 统一API的响应格式，确保前后端之间的数据交互更加规范和一致。
 *
 * @author 陈亮、梁旭、CokiLee
 * @version 1.0
 * @since 2024-10-16
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)  // 序列化对象为JSON时，只包含值不为null的字段
public class Result<T> {

    private Integer status_code;
    private String msg;
    private T data;

    public void status_code(Integer status_code) {
        this.status_code = status_code;
    }

    //直接返回成功状态码
    public static <T> Result<T> success(){
        Result<T> result=new Result<>();
        result.setStatus_code(1);
        return result;
    }

    //返回成功状态码的同时返回对象
    public static <T> Result<T> success(T data){
        Result<T> result=new Result<>();
        result.setStatus_code(1);
        result.setData(data);
        return result;
    }

    //直接返回错误状态码和错误信息
    public static <T> Result<T> fail(ErrorMsg errorMsg){
        Result<T> result=new Result<>();
        result.setStatus_code(0);
        result.setMsg(errorMsg.getMsg());
        return result;
    }

    //返回错误状态码和错误信息的同时返回错误对象
    public static <T> Result<T> fail(ErrorMsg errorMsg,T data){
        Result<T> result=new Result<>();
        result.setStatus_code(0);
        result.setMsg(errorMsg.getMsg());
        result.setData(data);
        return result;
    }
}
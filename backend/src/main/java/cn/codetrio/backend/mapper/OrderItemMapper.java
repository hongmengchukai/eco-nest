package cn.codetrio.backend.mapper;

import cn.codetrio.backend.entity.OrderItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author CokiLee
 * @version 1.0
 * @since 2024-10-15
 */

@Mapper
public interface OrderItemMapper extends BaseMapper<OrderItem> {
}

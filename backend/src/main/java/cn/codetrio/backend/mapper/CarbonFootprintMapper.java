package cn.codetrio.backend.mapper;

import cn.codetrio.backend.entity.CarbonFootprint;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 梁旭
 * @version 1.0
 * @since 2024-10-14
 */

@Mapper
public interface CarbonFootprintMapper extends BaseMapper<CarbonFootprint> {
}

package cn.codetrio.backend.mapper;

import cn.codetrio.backend.entity.Product;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 陈亮
 * @version 1.0
 * @since 2024-10-14
*/

@Mapper
public interface ProductMapper extends BaseMapper<Product> {
}

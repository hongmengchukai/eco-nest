package cn.codetrio.backend.config;

import cn.codetrio.backend.util.ErrorMsg;
import cn.codetrio.backend.util.ParamException;
import cn.codetrio.backend.util.Result;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestCookieException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 如果一个请求在经过Spring框架处理之前就要处理（或者说能够/可以被处理），
 * 那么这种情况一般选择用过滤器，例如：对请求URL做限制，限制某些URL的请求不被接受，
 * 这个动作是没有必要经过Spring的，直接过滤器初始化规则过滤即可；
 * 再比如：对请求数据做字符转换，请求的是密文，转换成明文，顺便再校验一下数据格式，
 * 这个也不需要经过Spring；总之，与详细业务不相关的请求处理都可以用过滤器来做；
 * 而与业务相关的自然就用拦截器来做，比如：对业务处理前后的数据做日志的记录；
 *
 * @author 陈亮
 * @version 1.0
 * @since 2024-10-17
 */

@ControllerAdvice
@ResponseBody
public class GlobalExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Result<Object> MethodArgumentNotValidExceptionHandler(MethodArgumentNotValidException e) {
        Map<String, String> collect = e.getBindingResult().getFieldErrors().stream()
                .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
        return Result.fail(ErrorMsg.PARAM_ERROR,collect);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public Result<Object> HttpMessageNotReadableExceptionHandler() {
        return Result.fail(ErrorMsg.MISSING_PARAMETER, "requestBody错误!");
    }

    // 返回首个缺少的参数名
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public Result<Object> MissingServletRequestParameterExceptionHandler(MissingServletRequestParameterException e) {
        return Result.fail(ErrorMsg.MISSING_PARAMETER, "缺少参数"+e.getParameterName());
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public Result<Object> ConstraintViolationExceptionHandler(ConstraintViolationException e) {

        Set<ConstraintViolation<?>> set = e.getConstraintViolations();
        Map<String, String> map = new HashMap<>();
        if (!set.isEmpty()) {
            for (ConstraintViolation<?> cv : set) {
                String[] param = cv.getPropertyPath().toString().split("\\.");
                String message = cv.getMessage();
                map.put(param[param.length - 1], message);
            }
        }

        return Result.fail(ErrorMsg.PARAM_ERROR, map);
    }

    @ExceptionHandler(ParamException.class)
    public Result<Object> ParamExceptionHandler(ParamException e) {
        return Result.fail(ErrorMsg.PARAM_ERROR, e.getMap());
    }

    /*@ExceptionHandler(Exception.class)
    public Object CommonExceptionHandler(Exception e){
        return "服务器错误";
    }*/

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
         @ResponseBody
         public Map<String, Object> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex) {
             Map<String, Object> response = new HashMap<>();
             response.put("message", "参数类型不匹配: " + ex.getMessage());
             response.put("status", HttpStatus.BAD_REQUEST.value());
             return response;
         }

    // 拦截cookie缺失异常
    @ExceptionHandler(MissingRequestCookieException.class)
    public Result<Object> MissingRequestCookieExceptionHandler(){
        return Result.fail(ErrorMsg.COOKIE_ERROR);
    }
}

/*
  DefaultHandlerExceptionResolver

  HttpRequestMethodNotSupportedException
  405 (SC_METHOD_NOT_ALLOWED)
  HttpMediaTypeNotSupportedException
  415 (SC_UNSUPPORTED_MEDIA_TYPE)
  HttpMediaTypeNotAcceptableException
  406 (SC_NOT_ACCEPTABLE)
  MissingPathVariableException
  500 (SC_INTERNAL_SERVER_ERROR)
  MissingServletRequestParameterException
  400 (SC_BAD_REQUEST)
  ServletRequestBindingException
  400 (SC_BAD_REQUEST)
  ConversionNotSupportedException
  500 (SC_INTERNAL_SERVER_ERROR)
  TypeMismatchException
  400 (SC_BAD_REQUEST)
  HttpMessageNotReadableException
  400 (SC_BAD_REQUEST)
  HttpMessageNotWritableException
  500 (SC_INTERNAL_SERVER_ERROR)
  MethodArgumentNotValidException
  400 (SC_BAD_REQUEST)
  MissingServletRequestPartException
  400 (SC_BAD_REQUEST)
  BindException
  400 (SC_BAD_REQUEST)
  NoHandlerFoundException
  404 (SC_NOT_FOUND)
  AsyncRequestTimeoutException
  503 (SC_SERVICE_UNAVAILABLE)
 */

package cn.codetrio.backend.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;

/**
 * @author 陈亮
 * @version 1.0
 * @since 2024-10-15
*/

// 处理数据对象（如实体类）在插入和更新时的字段填充逻辑
@Configuration
public class MyMetaObjectHandler implements MetaObjectHandler {

    // 新增数据时，填充字段
    @Override
    public void insertFill(MetaObject metaObject) {
        this.setFieldValByName("createdAt", LocalDateTime.now() ,metaObject);
        this.setFieldValByName("listedAt", LocalDateTime.now() ,metaObject);
        this.setFieldValByName("addAt", LocalDateTime.now() ,metaObject);
        this.setFieldValByName("commentTime", LocalDateTime.now() ,metaObject);
    }

    // 更新数据时，填充字段
    @Override
    public void updateFill(MetaObject metaObject) {

    }
}
